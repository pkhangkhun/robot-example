## GET /system-user/v1/users

#### Request headers (optional)

```
Content-Type: "application/json"
Authorization: "Bearer tokendata"
```

#### Response body

* Status: 200
* Content-Type: "application/json"

```json
{
    "code": "success",
    "data": {
        "username": "user01",
        "fullname": "user-user01",
        "is_teller": true,
        "branch_id": "00199",
        "branch_name_th": "xxx",
        "branch_name_en": "xxx",
        "teller_id": "xxx"
    }
}
```

---

* Status: 401
* Content-Type: "application/json"

```
Unauthorized
```
