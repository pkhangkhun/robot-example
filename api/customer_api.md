## POST /customer/v1/customers/identity/search

#### Request headers (optional)

```
Content-Type: "application/json"
```

#### Request body

```json
{
  "identity": "123",
  "identity_type": "CITIZEN",
  "issuer_country_code": "THA"
}
```

#### Response body

```json
{
    "code": "success",
    "data": {
        "cif": "cif123",
        "name": "name",
        "date_of_birth": "2021-11-11T21:32:04.522Z",
        "government_identity_id": "123",
        "government_identity_type": "CITIZEN",
        "government_country": "THA",
        "customer_type": "Individual",
        "customer_category": "Individuals",
        "country_of_citizenship": "TH",
        "customer_segment_code": "CommercialBankingGroup",
        "customer_created_datetime": "2021-11-11T21:32:04.522Z"
    }
}
```