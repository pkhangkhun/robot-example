## POST /v1/oauth/token

#### Request headers (optional)

```
Content-Type: "application/json"
```

#### Request body

```json
{
    "system_user_identity": "user01",
    "passcode": "pass"
}
```

#### Response body

* Status: 200
* Content-Type: "application/json"

```json
{
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6InVzZXIwMSIsImZ1bGxuYW1lIjoidXNlci11c2VyMDEiLCJicmFuY2hfaWQiOiIwMDE5OSIsImlzX3RlbGxlciI6dHJ1ZSwiaWF0IjoxNjM2NjY2MDkyfQ.Gy-c1aMPxeZdqZvG_bGgOKMdSLMGCGiSHnIW4m64aOU",
    "token_type": "Bearer"
}
```

---

* Status: 401
* Content-Type: "application/json"

```json
{
    "code": "general_error",
    "data": {
        "message": "Password is incorrect"
    }
}
```
