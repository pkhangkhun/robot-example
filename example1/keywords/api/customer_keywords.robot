*** Settings ***
Documentation    Customer API keywords
Resource    ../../resources/imports.robot

*** Keywords ***
Call POST search customer
    [Arguments]    ${request_body}
    Create Session    postCustomerSchApi    ${sample_service.service_url}

    ${headers}=     Create dictionary    Content-Type=application/json
    ${response}=    POST On Session    postCustomerSchApi    ${customer_api.search}    json=${request_body}   headers=${headers}
    ${resp_json}=    Set variable    ${response.json()}
    set test variable    ${response_body}    ${resp_json['data']}
    set test variable    ${json_response}    ${resp_json}
    set test variable    ${response}    ${response}

Call POST search customer with field
    [Arguments]    ${identity}      ${identity_type}     ${issuer_country_code}     ${expected_status}=200
    Create Session    postCustomerSchApi    ${sample_service.service_url}
    ${headers}=     Create dictionary    Content-Type=application/json
    ${request_body}=    Create Dictionary
    Run keyword if    "${identity}"!="${None}"              set to dictionary    ${request_body}    identity                ${identity}
    Run keyword if    "${identity_type}"!="${None}"         set to dictionary    ${request_body}    identity_type           ${identity_type}
    Run keyword if    "${issuer_country_code}"!="${None}"   set to dictionary    ${request_body}    issuer_country_code     ${issuer_country_code}

    ${response}=    POST On Session    postCustomerSchApi    ${customer_api.search}    json=${request_body}   headers=${headers}    expected_status=${expected_status}
    ${resp_json}=    Set variable    ${response.json()}
    set test variable    ${response_body}    ${resp_json['data']}
    set test variable    ${json_response}    ${resp_json}
    set test variable    ${response}    ${response}

Validate customer response body
    [Arguments]      ${response_body}      ${expected_body}
    should be equal    ${response_body['cif']}    ${expected_body.cif}
    AND should be equal    ${response_body['name']}    ${expected_body.name}
    AND should be equal    ${response_body['government_identity_id']}    ${expected_body.government_identity_id}
    AND should be equal    ${response_body['government_identity_type']}    ${expected_body.government_identity_type}
