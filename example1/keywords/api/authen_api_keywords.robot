*** Settings ***
Documentation    Customer API keywords
Resource    ../../resources/imports.robot

*** Keywords ***
Call POST authen
    [Arguments]    ${user}      ${pass}   ${expected_status}=200
    Create Session    authApi    ${sample_service.service_url}
    ${headers}=     Create dictionary    Content-Type=application/json
    ${request_body}=    Create Dictionary
    set to dictionary    ${request_body}    system_user_identity     ${user}
    set to dictionary    ${request_body}    passcode     ${pass}

    ${response}=    POST On Session    authApi    ${authen_api.authn}    json=${request_body}   headers=${headers}    expected_status=${expected_status}
    ${resp_json}=    Set variable    ${response.json()}
    set test variable    ${json_response}    ${resp_json}
    set test variable    ${response}    ${response}


Call POST authen success
    [Arguments]    ${user}      ${pass}
    Create Session    authApi    ${sample_service.service_url}
    ${headers}=     Create dictionary    Content-Type=application/json
    ${request_body}=    Create Dictionary
    set to dictionary    ${request_body}    system_user_identity     ${user}
    set to dictionary    ${request_body}    passcode     ${pass}

    ${response}=    POST On Session    authApi    ${authen_api.authn}    json=${request_body}   headers=${headers}    expected_status=${expected_status}
    ${resp_json}=    Set variable    ${response.json()}
    set test variable    ${token}    ${resp_json['access_token']}
