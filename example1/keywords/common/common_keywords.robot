*** Settings ***
Resource    ../../resources/imports.robot

*** Keywords ***
Generate Headers
    [Arguments]         ${access_key}
    ${correlation_id}    common_util.generate_uuid    ${application.name}
    ${headers}    create dictionary    Content-Type=application/json
    ...  X-ACCESS-KEY=${access_key}    X-Correlation-ID=${correlation_id}
    [Return]    ${headers}

Set data to "${datas}"
    Set test variable   ${datas}     ${datas}

HTTP status should be '${expected_http}'
    [Documentation]    Verify response http status with expected code
    should be equal as integers    ${response.status_code}    ${expected_http}

Response code should be
    [Arguments]    ${expected_code}
    [Documentation]    Verify response with expected code
    should be equal    ${json_response['code']}    ${expected_code}

Load json data '${json_variable}'
    ${json_param}    Set Variable    ${json_variable}
    # set test variable    ${json_temp}    ${json_variable}
    Set test variable    ${json_param}

Change parameter '${json_path}' to '${value}'
    JSONLibrary.Update Value To Json    ${json_temp}    ${json_path}    ${value}
    ${json_param}    Convert JSON To String    ${json_temp}
    Set test variable    ${json_param}    ${json_temp}
