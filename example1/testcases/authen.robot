*** Settings ***
Resource     ../resources/imports.robot
Resource     ../keywords/api/authen_api_keywords.robot

*** Test Cases ***
TC_ATH_00001 Authen success
    [Documentation]  Authentication service return success
    [Tags]    regression     high     smoke_test

    When Call POST authen    user01     myPassw0rd
    Then HTTP status should be '200'
    And should be equal     ${json_response['token_type']}    Bearer
    And variable should exist   ${json_response['access_token']}

TC_ATH_00002 Authen fail - missing password
    [Documentation]  Authentication service return success
    [Tags]    regression     high     smoke_test

    When Call POST authen    user01     ${None}     expected_status=401
    Then HTTP status should be '401'
    And Response code should be    general_error
    And should be equal     ${json_response['data']['message']}    Password is incorrect
