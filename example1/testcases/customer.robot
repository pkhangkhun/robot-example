*** Settings ***
Resource     ../resources/imports.robot
Resource     ../keywords/api/customer_keywords.robot
Variables    ../resources/testdata/${ENV}/customer.yaml

*** Test Cases ***
TC_00001 Customer Search Success - Input valid information
    [Documentation]  Search customer api with invalid input
    [Tags]    regression     high     smoke_test

    When Call POST search customer    ${customer_data.cust01.rq}
    Then HTTP status should be '200'
    And Response code should be     success
    And Validate customer response body     ${response_body}        ${customer_data.cust01.rs}

TC_00002 Customer Search Success - Input new customer
    [Documentation]  Search customer api with invalid input
    [Tags]    regression     high     smoke_test

    When Call POST search customer with field
    ...    ${customer_data.cust02.rq.identity}
    ...    ${customer_data.cust02.rq.identity_type}
    ...    ${customer_data.cust02.rq.issuer_country_code}
    Then HTTP status should be '200'
    And Response code should be     success
    And Validate customer response body     ${response_body}        ${customer_data.cust02.rs}

TC_00003 Customer Search Fail - invalid id
    [Documentation]  Search customer api with invalid id
    [Tags]    regression     high     smoke_test

    When Call POST search customer with field
    ...    ${customer_data.cust03.rq.identity}
    ...    ${customer_data.cust03.rq.identity_type}
    ...    ${customer_data.cust03.rq.issuer_country_code}
    ...    expected_status=404
    Then HTTP status should be '404'
    And Response code should be    ${customer_data.cust03.rs.code}
